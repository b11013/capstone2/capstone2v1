const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        required: [true, "First Name is required."],
    },

    lastName: {
        type: String,
        required: [true, "Last Name is required."],
    },

    email: {
        type: String,
        required: [true, "EMAIL is required."],
    },

    password: {
        type: String,
        required: [true, "Password is required."],
    },
    activeStatus: {
        type: Boolean,
        default: true,
    },
    isAdmin: {
        type: Boolean,
        default: false,
    },
    orders: [{
        productId: { type: String, required: [true, "Product ID is required."] },
        price: { type: Number, required: [true, "Product ID is required."] },
        createdOn: { type: Date, default: new Date() },
    }, ],
    totalAmount: [{
        type: Number,
        default: 0,
    }, ],
    totalAmountOfOrders: Number,
});

module.exports = mongoose.model("User", userSchema);