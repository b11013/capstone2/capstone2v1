const Product = require("../models/Product");
const bcrypt = require("bcryptjs");
const auth = require("../auth");

// Add Product
module.exports.addProduct = (req, res) => {
    req.body;

    let newProduct = new Product({
        softwareName: req.body.softwareName,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category,
    });
    Product.findOne({ softwareName: req.body.softwareName })
        .then((result) => {
            if (result !== null && result.softwareName === req.body.softwareName) {
                return res.send(
                    req.body.softwareName + " is already registered in the products!"
                );
            } else {
                newProduct.save().then((product) =>
                    res.send({
                        Alert: "Product registered successfully!",
                        Info: product,
                    })
                );
            }
        })
        .catch((error) => res.send(error));
};

//view all products
module.exports.getAllProducts = (req, res) => {
    Product.find({})
        .then((result) => res.send(result))
        .catch((err) => res.send(err));
};

//view single product
module.exports.getSingleProduct = (req, res) => {
    Product.findById(req.params.id)
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//Edit a Product Info
module.exports.editProduct = (req, res) => {
    let updates = {
        softwareName: req.body.softwareName,
        description: req.body.description,
        price: req.body.price,
        category: req.body.category,
    };

    Product.findByIdAndUpdate(req.params.id, updates, { new: true })
        .then((result) =>
            res.send({
                Alert: req.body.softwareName + " edited successfully. ",
                Info: result,
            })
        )
        .catch((error) => res.send(error));
};

//Deactivate/Archive A Product
module.exports.archiveProduct = (req, res) => {
    let archive = {
        isActive: false,
    };

    Product.findByIdAndUpdate(req.params.id, archive, { new: true })
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//ACTIVATE A Product
module.exports.activateProduct = (req, res) => {
    let activate = {
        isActive: true,
    };

    Product.findByIdAndUpdate(req.params.id, activate, { new: true })
        .then((result) => res.send(result))
        .catch((error) => res.send(error));
};

//View All Active Product
module.exports.viewActiveProduct = (req, res) => {
    let viewAllActiveProducts = [];
    Product.find({ isActive: true })
        .then((result) => {
            result.forEach((item) => {
                viewAllActiveProducts.push({
                    softwareName: item.softwareName,
                    description: item.description,
                    price: item.price,
                    category: item.category,
                });
            });

            res.send({ viewAllActiveProducts });
        })
        .catch((err) => res.send(err));
};

//View Product per category
module.exports.viewPerCategory = (req, res) => {
    Product.find({ category: req.body.category })
        .then((result) => res.send(result))
        .catch((err) => res.send("Please check your spelling" + err));
};