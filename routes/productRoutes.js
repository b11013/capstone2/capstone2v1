const express = require("express");
const router = express.Router();

const productControllers = require("../controllers/productControllers");

const auth = require("../auth");

const { verify } = auth;
const { verifyAdmin } = auth;

//add products
router.post("/", verify, verifyAdmin, productControllers.addProduct);

//view all products
router.get("/", productControllers.getAllProducts);

//view single product
router.get("/getSingleProduct/:id", productControllers.getSingleProduct);

//edit a product using ID
router.put("/:id", verify, verifyAdmin, productControllers.editProduct);

//Archive a product
router.put(
    "/archive/:id",
    verify,
    verifyAdmin,
    productControllers.archiveProduct
);

//Activate a product
router.put(
    "/activate/:id",
    verify,
    verifyAdmin,
    productControllers.activateProduct
);

//Get all Active products
router.get("/getActiveProduct", productControllers.viewActiveProduct);

//view products per category
router.get("/viewProductsPerCategory", productControllers.viewPerCategory);

module.exports = router;